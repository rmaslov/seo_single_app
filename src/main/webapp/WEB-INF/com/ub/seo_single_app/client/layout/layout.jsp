<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Single App Application</title>
    <style>.navbar-fixed {height: 64px;background-color: #03a9f4;}body{margin:0px;}</style>
</head>
<body>
<div class="navbar-fixed js-navbar"></div>

<div class="js-main-body"></div>

<footer class="js-page-footer page-footer orange hide"></footer>


<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/static/ssa/css/main.css" rel="stylesheet" type="text/css" media="screen,projection"/>
<script src="/static/ssa/dist/main.js" async></script>
</body>
</html>
