package com.ub.seo_single_app.index.controllers;

import com.ub.ubblog.blog.services.BlogService;
import com.ub.ubblog.blog.views.all.SearchBlogAdminRequest;
import com.ub.ubblog.blog.views.all.SearchBlogAdminResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@Controller
public class IndexController {
    @Autowired private BlogService blogService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index( HttpServletResponse response) {
        return "com.ub.seo_single_app.client.template";
    }

    @RequestMapping(value = "/blog/p{index}", method = RequestMethod.GET)
    public String index(HttpServletResponse response, @PathVariable Integer index) {
        return "com.ub.seo_single_app.client.template";
    }

    @RequestMapping(value = "/blog/{url}", method = RequestMethod.GET)
    public String index(HttpServletResponse response, @PathVariable String url) {
        return "com.ub.seo_single_app.client.template";
    }


    @ResponseBody
    @RequestMapping(value = "/api/v1/blog/all", method = RequestMethod.GET)
    public SearchBlogAdminResponse blogAll(HttpServletResponse response, @RequestParam Integer page) {
        SearchBlogAdminRequest blogRequest = new SearchBlogAdminRequest(page, 18);
        return blogService.findAll(blogRequest);
    }

}
